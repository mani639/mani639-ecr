provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    encrypt = true
    bucket = "mani639-terraform-state-storage-s3"
    region = "us-east-2"
    key = "terraform-state-ecs/terraform.tfstate"
  }
}

data "aws_iam_role" "ecr" {
  name = "AWSServiceRoleForECRReplication"
}

module "ecr" {
  source = "cloudposse/ecr/aws"
  # Cloud Posse recommends pinning every module to a specific version
  # version     = "x.x.x"
  namespace              = "${var.namespace}"
  stage                  = "${var.stage}"
  name                   = "${var.ecr_name}"
  principals_full_access = [data.aws_iam_role.ecr.arn]
}
