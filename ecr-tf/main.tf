provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    encrypt = true
    bucket = "mani639-terraform-state-storage-s3"
    region = "us-east-2"
    key = "terraform-state-ecr/terraform.tfstate"
  }
}

module "ecs" {
  source = "terraform-aws-modules/ecs/aws"

  name = "${var.ecs_name}"

  capacity_providers = "${var.capacity_providers}"

  tags = {
    Environment = "${var.Environment}"
  }
}

module "ecs-fargate-task-definition" {
  source  = "cn-terraform/ecs-fargate-task-definition/aws"
  # insert the 25 required variables here
  name_prefix = "${var.fargate_name_prefix}"
  container_name = "${var.container_name}"
  container_image = "${var.container_image}"
  port_mappings = [{
    containerPort = "${var.containerPort}"
    hostPort      = "${var.hostPort}"
    protocol      = "${var.protocol}"
  }]

}

module "ecs-fargate-scheduled-task" {
  source = "umotif-public/ecs-fargate-scheduled-task/aws"

  name_prefix = "${var.fargate_schedule_name_prefix}"

  ecs_cluster_arn = "${var.ecs_cluster_arn}"

  task_role_arn      = "${var.task_role_arn}"
  execution_role_arn = "${var.execution_role_arn}"

  event_target_task_definition_arn = "${var.event_target_task_definition_arn}"
  event_rule_schedule_expression   = "${var.event_rule_schedule_expression}"
  event_target_task_count          = "${var.event_target_task_count}"
  event_target_subnets             = "${var.event_target_subnets}"
}
